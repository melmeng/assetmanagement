#-------------------------------------------------------------------------------
# Name:        calculation_util
# Purpose: basic normal distribution manipulation
#
# Author:      ymeng
#
# Created:     21/02/2013
# Copyright:   (c) ymeng 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------


import math



def get_normal_value(mu, sigma, x):
    """
    p(x) = N(mu, sigma)  
    """
    v = 1.0/sigma/(2*math.pi)**0.5*math.exp(-(x-mu)**2/2.0/sigma**2)

    return v

def init(mu, sigma):
    return mu, sigma

def move(mu, sigma, m_mu, m_sigma):
    """
    V = N(mu, sigma) + N(m_mu, m_sigma)
    """
    v = mu + m_mu, sigma + m_sigma
    print 'move mu, sigma: %s,%s => %s, %s' % (mu, sigma, m_mu, m_sigma)
    print 'updated mu:%s, sigma: %s' % v
    return v

def update(mu, sigma, u_mu, u_sigma):
    """
    V = N(mu, sigma)XN(u_mu, u_sigma)
    """
    v = (mu*u_sigma**2 + u_mu*sigma**2)/(sigma**2 + u_sigma**2), (1.0/(1.0/sigma**2 + 1.0/u_sigma**2))**0.5
    print 'update mu, sigma: %s,%s with measurement %s, %s' % (mu, sigma, u_mu, u_sigma)
    print 'updated mu:%s, sigma: %s' % v
    return v