class SuperState( object ):
    def predict(self):
        raise NotImplementedError()
    def update(self):
        raise NotImplementedError()
    def transitionRule( self, input ):
        raise NotImplementedError()