from Observer import Observer
from superstate import SuperState


class RiskManager(Observer, SuperState):
    map = []
    def __init__(self):
        self.events = []
        self.mu = None
        self.sigma = None
        
    def predict(self, event):
        self.fireEventName('beforepredict')
        self.fireEventName('onpredict')
        self.mu, self.sigma = event['fn'](self.mu, self.sigma, event['mu'], event['sigma'])
        self.events.append({'event': event, 'mu': self.mu, 'sigma': self.sigma})
        self.fireEventName('afterpredict', {'event': event, 'mu': self.mu, 'sigma': self.sigma})
    
    def update(self, event):
        self.fireEventName('beforeupdate')
        self.fireEventName('onupdate')
        
        self.mu, self.sigma = event['fn'](self.mu, self.sigma, event['mu'], event['sigma'])
        self.events.append({'event': event, 'mu': self.mu, 'sigma': self.sigma})
        self.fireEventName('afterupdate', {'event': event, 'mu': self.mu, 'sigma': self.sigma})
        
    def event(self, event):
        action = event['action']
        if action=='init':
            self.mu = event['mu']
            self.sigma = event['sigma']
        elif action=='move':
            self.predict(event)
        elif action=='update':
            self.update(event)

    def rollback(self, timestamp=None):
        self.fireEventName('beforerollback')
        if timestamp:
            pass
        else:
            #pop up the last event
            self.events.pop()
            if len(self.events)==0:
                #no history
                self.mu = None
                self.sigma = None
            else:
                event = self.events[-1]
                self.mu = event['mu']
                self.sigma = event['sigma']
            self.fireEventName('onrollback')
            self.fireEventName('afterrollback')
        
    