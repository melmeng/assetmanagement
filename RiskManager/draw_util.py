import numpy
import pylab
from calculation_util import get_normal_value



class NormalCurvePlotter:
    def __init__(self, mu=None, sigma=None, interactive=True, grid=True):
        if interactive:
            pylab.ion()
        if grid:
            pylab.grid(True)
            
        figure = pylab.Figure()
        ax = figure.add_subplot(1,1,1)
        self.figure = ax
        self.mu = mu
        self.sigma = sigma
    def draw(self, title=''):
        return draw_normal_curve(self.mu, self.sigma, title)
    def fill(self, interval=2, facecolor='blue', alpha=0.5):
        return fill_normal_curve(self.mu, self.sigma, interval, facecolor, alpha)
    def label(self, label):
        mu = self.mu
        sigma = self.sigma
        lines = pylab.scatter([mu], [get_normal_value(mu, sigma, mu)], marker='o')
        pylab.annotate(label, xy=(mu, get_normal_value(mu, sigma, mu)))
        
        return lines
        
    
    def save(self, out):
        pylab.savefig(out)
    def clear(self):
        pylab.clf()
    def update(self, mu, sigma, clear=False):
        if clear:
            self.clear()
        self.mu = mu
        self.sigma = sigma
    def close(self):
        pylab.close()

def draw_normal_curve(mu, sigma, title=''):
    pylab.title(title)
    t = numpy.linspace(mu-sigma*5,mu+sigma*5,101)
    
    v = [get_normal_value(mu, sigma, x) for x in t]
    lines = pylab.plot(t, v)
    pylab.draw()
    
    return lines
        
def fill_normal_curve(mu, sigma, f=2, facecolor='blue', alpha=0.5):
    t = numpy.linspace(mu-sigma*5,mu+sigma*5,101)
    v = [get_normal_value(mu, sigma, x) for x in t]
    
    x0 = []
    y0 = []
    for i in range(len(t)):
        if t[i]>mu - sigma*f and t[i]<mu + sigma*f:
            x0.append(t[i])
            y0.append(v[i])
    
    t = [x0[0]] + x0 + [x0[-1]]
    v = [0] + y0 + [0]
    lines = pylab.fill(t , v, facecolor=facecolor, alpha=alpha)
    pylab.draw()
    return lines
    