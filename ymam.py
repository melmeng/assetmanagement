#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      ymeng
#
# Created:     21/02/2013
# Copyright:   (c) ymeng 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import numpy
import pylab
import math

def get_normal_value(mu, sigma, x):
    v = 1.0/sigma/(2*math.pi)**0.5*math.exp(-(x-mu)**2/2.0/sigma**2)

    return v

def move(mu, sigma, m_mu, m_sigma):
    v = mu + m_mu, sigma + m_sigma
    print 'move mu, sigma: %s,%s => %s, %s' % (mu, sigma, m_mu, m_sigma)
    print 'updated mu:%s, sigma: %s' % v
    return v

def update(mu, sigma, u_mu, u_sigma):
    v = (mu*u_sigma**2 + u_mu*sigma**2)/(sigma**2 + u_sigma**2), (1.0/(1.0/sigma**2 + 1.0/u_sigma**2))**0.5
    print 'update mu, sigma: %s,%s with measurement %s, %s' % (mu, sigma, u_mu, u_sigma)
    print 'updated mu:%s, sigma: %s' % v
    return v


def draw(mu, sigma, title='', out=None):
    pylab.ion()
    pylab.grid(True)
    
    title+='mu, sigma = (%s, %s)' % (mu, sigma)
    pylab.title(title)
    t = numpy.linspace(mu-sigma*5,mu+sigma*5,101)
    
    v = [get_normal_value(mu, sigma, x) for x in t]
    pylab.plot(t, v)
    if out:
        pylab.savefig(out)
        
    pylab.scatter([mu], [get_normal_value(mu, sigma, mu)], marker='o')
    pylab.annotate(title, xy=(mu, get_normal_value(mu, sigma, mu)))
    
    pylab.draw()
    
    return t, v
        
def fill(mu, sigma, f=2, facecolor='blue', alpha=0.5):
    t = numpy.linspace(mu-sigma*5,mu+sigma*5,101)
    v = [get_normal_value(mu, sigma, x) for x in t]
    
    x0 = []
    y0 = []
    for i in range(len(t)):
        if t[i]>mu - sigma*f and t[i]<mu + sigma*f:
            x0.append(t[i])
            y0.append(v[i])
    
    t = [x0[0]] + x0 + [x0[-1]]
    v = [0] + y0 + [0]
    pylab.fill(t , v, facecolor=facecolor, alpha=alpha)
    pylab.draw()
#    pylab.show()
    
def simulate(sequences):
    while True:
#        raw_input('press to continue')
        import time
        if raw_input('Quit(Y|N):').upper()=='Y':
            break
        year = raw_input('press to enter time:')
        action = raw_input('1. init or 2. update or 3. move:')
        imu = float(raw_input('enter mu:'))
        isigma = float(raw_input('enter sigma:'))
        
        action = ['init', 'update', 'move'][int(action)-1]
        s = {'action': action, 'year': year, 'p': (imu, isigma)}
        


        if s['action']=='init':
            mu, sigma = s['p']
            out = '%s.jpg' % s['year']
        elif s['action']=='update':
            m_mu, m_sigma = s['p']
            draw(m_mu, m_sigma, title='measurement')
            mu, sigma = update(mu, sigma, m_mu, m_sigma)
        elif s['action']=='move':
            m_mu, m_sigma = s['p']
            mu, sigma = move(mu, sigma, m_mu, m_sigma)

        title =  'Year: %(year)s, %(action)s: mu, sigma = %(p)s' % s
        print title
        
        draw(mu, sigma, title=title, out=out)
        fill(mu, sigma)
        
        
        
#        pylab.savefig('%s.jpg' % s['year'])



    


sequences = [{'year': 2000, 'p': (2050, 10), 'action': 'init'},
             {'year': 2001, 'p': (0, 3), 'action': 'move'},
             {'year': 2002, 'p': (0, 3), 'action': 'move'},
             {'year': 2003, 'p': (0, 3), 'action': 'move'},
             {'year': 2004, 'p': (2040, 15), 'action': 'update'},
             {'year': 2004, 'p': (2040, 15), 'action': 'update'}
             ]
simulate(sequences)
###installed in 2000
##initial_state = (2050, 10) #mu, sigma, 50 year life and 10 year uncertainty
##mu, sigma = initial_state
##t = numpy.linspace(2000,2100,101)
##v = [get_normal_value(mu, sigma, x) for x in t]
##pylab.plot(t, v)
##
###2001, no inspection, sigma increase 3 yr
##u_mu, u_sigma = 0, 3
##v = [get_normal_value(u_mu, u_sigma, x) for x in t]
##pylab.plot(t, v, '-')
##mu, sigma = move(mu, sigma, u_mu, u_sigma)
##
##v = [get_normal_value(mu, sigma, x) for x in t]
##pylab.plot(t, v)
##
###2002, no inspection, sigma increase 3 yr
##mu, sigma = move(mu, sigma, 0, 3)
##
##v = [get_normal_value(mu, sigma, x) for x in t]
##pylab.plot(t, v)
##
###2003, inspection
##
##mu, sigma = update(mu, sigma, 2040, 20)
##
##v = [get_normal_value(mu, sigma, x) for x in t]
##pylab.plot(t, v, '*')
##
##
##pylab.show()
##
####
####
####def main():
####    t = numpy.linspace(2000,2100,101)
####    v = [get_normal_value(2050, 10, x) for x in t]
####    pylab.plot(t, v)
####
####    v = [get_normal_value(2050, 15, x) for x in t]
####    pylab.plot(t, v)
####
####    pylab.show()
####
####
####
####
####if __name__ == '__main__':
####    main()
