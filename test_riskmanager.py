import unittest
import random
from RiskManager import *


class TestSequenceFunctions(unittest.TestCase):

    def setUp(self):
        self.np = draw_util.NormalCurvePlotter(10, 5)

    def test_draw(self):
        # make sure the shuffled sequence does not lose any elements
        self.np.draw(title="First Test")
        self.np.fill()
        self.np.label('N(10,5)')
        self.np.save('./test.jpg')
    def test_rm_events(self):
        rm = RiskManager()
        np = draw_util.NormalCurvePlotter()
        def showevent(e):
            print e.name
            event = e.stuff
            np.update(event['mu'], event['sigma'])
            line = np.draw()
            fill = np.fill()
            
        
        rm.register('afterupdate', showevent)
        rm.register('afterpredict', showevent)
        
        events =[ 
        {'action': 'init', 'fn': calculation_util.init, 'mu': 3.5, 'sigma': 0.5, 'time': '2013'},
        {'action': 'move', 'fn': calculation_util.move, 'mu': 0.5, 'sigma': 1, 'time': 2014},
        {'action': 'move', 'fn': calculation_util.update, 'mu': 4.5, 'sigma': 0.5, 'time': 2014}]
        for event in events:
            rm.event(event)
        
        
        
        
#
#        # should raise an exception for an immutable sequence
#        self.assertRaises(TypeError, random.shuffle, (1,2,3))
#
#    def test_choice(self):
#        element = random.choice(self.seq)
#        self.assertTrue(element in self.seq)
#
#    def test_sample(self):
#        with self.assertRaises(ValueError):
#            random.sample(self.seq, 20)
#        for element in random.sample(self.seq, 5):
#            self.assertTrue(element in self.seq)

if __name__ == '__main__':
    unittest.main()