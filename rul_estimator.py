import numpy as np
import matplotlib.pyplot as plt



def rul(a, c=None):
    '''regression equation for rul based on age and condtion
    '''
    
    if c==5:
        return 10
    if c==4:
        return 20
    return -0.0000132917*a**3 + 0.00628687*a**2-0.12303*a +100 - a 


def plot_rul_curve():
    x = np.linspace(0, 200, 200)
    y = [rul(a) for a in x]
    y1 = [0.8*a for a in y]
    y2 = [1.2*a for a in y]
    
    plt.plot(x, y)
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.grid()
    plt.show()
    plt.close()

def mc_rul(age, condtion, sigma):
    '''uncertainty caused by age estimation'''
    age = np.random.normal(age, sigma, 10000)
    print 'age mean:', np.mean(age)
    print 'age sigma', np.std(age)
    #adding 20% normal uncertained to the rul estimation.
    s = [np.random.normal(rul(x), 0.2*rul(x), 1)[0] for x in age]
    
    sigma = np.std(s)
    mu = np.mean(s)
    print 'rul mean:', np.mean(s)
    print 'rul sigma', np.std(s)
    count, bins, ignored = plt.hist(s, 200, normed=True)
    plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *np.exp( - (bins - mu)**2 / (2 * sigma**2) ),linewidth=2, color='r')
    plt.plot()
    
    s = [rul(x) for x in age]
    sigma = np.std(s)
    mu = np.mean(s)
    #count, bins, ignored = plt.hist(s, 100, normed=True)
    plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *np.exp( - (bins - mu)**2 / (2 * sigma**2) ),linewidth=2, color='g')
    plt.plot()
    
    plt.show()
    plt.close()


    
plot_rul_curve()

age = 60
condition = 1
sigma = 5

#if age has uncertainty in it, say +/-5 years, what is the rul look like
#bars and red is by assuming the rul model has 0.2rul uncertainty
#green shows that the rul curve is exact
mc_rul(age, condition, sigma)