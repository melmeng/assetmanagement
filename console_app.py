from RiskManager import *
rm = RiskManager()
np = draw_util.NormalCurvePlotter()
def showevent(e):
    print e.name
    event = e.stuff
    np.update(event['mu'], event['sigma'])
    line = np.draw()
    fill = np.fill()
#rm.register('afterupdate', showevent)
#rm.register('afterpredict', showevent)

LINES = []    
def plot(rm):
    np.update(rm.mu, rm.sigma)
    line = np.draw()
    fill = np.fill()
    label = np.label('N(%s,%s)' % (rm.mu, rm.sigma))
    LINES.append((line[0], fill[0]))
    np.save('tmp.jpg')


events =[ 
{'action': 'init', 'fn': calculation_util.init, 'mu': 3.5, 'sigma': 0.5, 'time': '2013'},
{'action': 'move', 'fn': calculation_util.move, 'mu': 0.5, 'sigma': 1, 'time': 2014},
{'action': 'move', 'fn': calculation_util.update, 'mu': 4.5, 'sigma': 0.5, 'time': 2014}]
#for event in events:
#    rm.event(event)
#        

e1 = events[0]
e2 = events[1]
e3 = events[2]

def add_event(event):
    rm.event(event)
    plot(rm)
    #remove old lines
    
