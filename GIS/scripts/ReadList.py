import arcpy
import json
import csv
values = arcpy.GetParameterAsText(0)
fields = arcpy.GetParameterAsText(1)
outpath = arcpy.GetParameterAsText(2)
fields = [x.replace('#', '').strip() for x in fields.split(';')]
results = []
with open(outpath, 'w') as o:
    writer = csv.writer(o, lineterminator='\n')
    
    for value in values.split(';'):
        result = dict(zip(fields, value.split('_')))
        
        writer.writerow([json.dumps(result)])
        results.append(json.dumps(result))

results = ';'.join(results)
                  
                      

arcpy.SetParameterAsText(3, results)

