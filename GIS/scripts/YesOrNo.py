import arcpy

input_value = arcpy.GetParameterAsText(0)
outpath  = arcpy.GetParameterAsText(5)
var_name  = arcpy.GetParameterAsText(3)

is_yes = "false"
is_no = "false"

if input_value.upper()=='Y':
    is_yes = "true"
    outpath+='->%s=Y' % var_name
    


if input_value.upper()=='N':
    is_no = "true"
    outpath+='->%s=N' % var_name

arcpy.SetParameterAsText(1, is_yes)
arcpy.SetParameterAsText(2, is_no)
arcpy.SetParameterAsText(4, outpath)
    
