import arcpy

input_value = arcpy.GetParameterAsText(0)

if input_value.upper()=='Y':
    is_yes = "true"
    is_no = "false"
else:
    is_yes = "false"
    is_no = "true"

arcpy.SetParameterAsText(1, is_yes)
arcpy.SetParameterAsText(2, is_no)
    
