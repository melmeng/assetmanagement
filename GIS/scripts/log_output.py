import arcpy
import json
import csv

import logging
logger = logging.getLogger()
hdlr = logging.FileHandler('log.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.DEBUG)
values = arcpy.GetParameterAsText(0)
msg = arcpy.GetParameterAsText(1)
if msg:
    pass
else:
    msg = values
logger.info(msg)
arcpy.AddWarning(msg)

