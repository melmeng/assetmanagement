#fn("%Capacity%")
#hydraulic capacity
def fn(x):
    if x>=80:
        y = 'low'
    if x<=50:
        y = 'high'
    else:
        y = 'medium'
    return y

#findField("%layer%", "%field%", "%workspace%")
def findField(fc, fi, ws):
    import arcpy
    arcpy.env.workspace = ws
    lst = arcpy.ListFields(fc)
    for f in lst:
        if f.name == fi:
            return "true"
    return "false"